﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NfcReader.Connector.GSCFile
{
    public class GSCFileReaderSysInfo : GSCFileReader
    {        
        public GSCFileReaderSysInfo(byte[] data)
            : base(data)
        {            
        }

        public string WeaponSerialNumber
        {
            get
            {
                return GetString(4, 16);
            }
        }

        public DateTime GSCInitiationDate
        {
            get
            {
                return GetTimestamp(20);
            }
        }

        public DateTime GSCCurrentDate
        {
            get
            {
                return GetTimestamp(24);
            }
        }

        public DateTime GSCRTCUpdateDate
        {
            get
            {
                return GetTimestamp(28);
            }
        }

        public string WeaponType
        {
            get
            {
                return GetByte(32).ToString();
            }
        }

        public string WeaponSubType
        {
            get
            {
                return GetByte(33).ToString();
            }
        }

        public float GSCBatteryLevel
        {
            get
            {
                return (float)(((float)GetWord(36)) / 100.0);
            }
        }

        public UInt16 GSCTemp
        {
            get
            {
                return (UInt16)(GetByte(38) + 50);
            }
        }

        public string SoldierID
        {
            get
            {
                return GetString(40, 16);
            }
        }

        public string SoldierName
        {
            get
            {
                return GetString(56, 20);
            }
        }
    }
}
