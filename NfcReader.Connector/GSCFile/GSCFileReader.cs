﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NfcReader.Connector.GSCFile
{
    public class GSCFileReader : IDisposable
    {
        protected string mFileName = string.Empty;
        protected byte[] mFileData = null;
        protected bool mIsFileExist = false;

        public GSCFileReader()
        {
        }

        public void DeleteFile()
        {
            if (mIsFileExist == true)
                File.Delete(mFileName);
        }

        public GSCFileReader(byte[] data)
        {
            //mFileName = pFileName;
            try
            {
                //if (File.Exists(pFileName) == true)
                //{
                //mFileData = File.ReadAllBytes(pFileName);
                if (data.Length > 0)
                {
                    mIsFileExist = true;
                    mFileData = data;
                }

                //}
                //else
                //    mIsFileExist = false;
            }
            catch (Exception ex)
            {
                mIsFileExist = false;
            }
        }

        public bool IsFileExist()
        {
            return mIsFileExist;
        }

        public DateTime GetTimestamp(int pPos)
        {
            if (mIsFileExist == false)
                throw new FileNotFoundException(mFileName + " file was not read. No data to read!");

            if (pPos + 3 >= mFileData.Length)
                throw new IndexOutOfRangeException();

            UInt64 DateTimeVal;
            DateTimeVal = (UInt64)(mFileData[pPos + 3] << 24);
            DateTimeVal += (UInt64)(mFileData[pPos + 2] << 16);
            DateTimeVal += (UInt64)(mFileData[pPos + 1] << 8);
            DateTimeVal += (UInt64)(mFileData[pPos + 0]);


            UInt64 Sec = 0, Min = 0, Hour = 0, Day = 0, Mon = 0, Year = 0;

            Sec = (UInt64)mFileData[pPos + 3] & 0x3F;
            Min = (((UInt64)mFileData[pPos + 2] & 0x0F) << 2) + (((UInt64)mFileData[pPos + 3] >> 6) & 0x03);
            Hour = (((UInt64)mFileData[pPos + 1] & 0x01) << 4) + (((UInt64)mFileData[pPos + 2] >> 4) & 0x0F);
            Day = ((UInt64)mFileData[pPos + 1] & 0x3E) >> 1;
            Mon = (((UInt64)mFileData[pPos + 0] & 0x03) << 2) + (((UInt64)mFileData[pPos + 1] & 0xC0) >> 6);
            Year = 2000 + (((UInt64)mFileData[pPos + 0] >> 2) & 0x3F);

            try
            {
                return new DateTime((int)Year, (int)Mon, (int)Day, (int)Hour, (int)Min, (int)Sec);
            }
            catch (Exception ex)
            {
                return DateTime.MinValue;
            }
        }


        //public DateTime GetTimestamp(int pPos)
        //{
        //    if (mIsFileExist == false)
        //        throw new FileNotFoundException(mFileName + " file was not read. No data to read!");

        //    if (pPos + 3 >= mFileData.Length)
        //        throw new IndexOutOfRangeException();

        //    UInt64 DateTimeVal;
        //    DateTimeVal = (UInt64)(mFileData[pPos + 2] << 24);
        //    DateTimeVal += (UInt64)(mFileData[pPos + 3] << 16);
        //    DateTimeVal += (UInt64)(mFileData[pPos + 0] << 8);
        //    DateTimeVal += (UInt64)(mFileData[pPos + 1]);


        //    UInt64 Sec = 0, Min = 0, Hour = 0, Day = 0, Mon = 0, Year = 0;

        //    Sec = DateTimeVal & 0x3F;
        //    DateTimeVal = DateTimeVal >> 6;
        //    Min = DateTimeVal & 0x3F;
        //    DateTimeVal = DateTimeVal >> 6;
        //    Hour = DateTimeVal & 0x1F;
        //    DateTimeVal = DateTimeVal >> 5;
        //    Day = DateTimeVal & 0x1F;
        //    DateTimeVal = DateTimeVal >> 5;
        //    Mon = DateTimeVal & 0x0F;
        //    DateTimeVal = DateTimeVal >> 4;
        //    Year = 2000 + (DateTimeVal & 0x3F);
        //    DateTimeVal = DateTimeVal >> 6;

        //    try
        //    {
        //        return new DateTime((int)Year, (int)Mon, (int)Day, (int)Hour, (int)Min, (int)Sec);
        //    }
        //    catch (Exception ex)
        //    {
        //        return DateTime.MinValue;
        //    }
        //}

        public Int16 GetWord(int pPos)
        {
            Int16 RetVal;

            if (mIsFileExist == false)
                throw new FileNotFoundException(mFileName + " file was not read. No data to read!");

            if (pPos + 1 >= mFileData.Length)
                throw new IndexOutOfRangeException();

            RetVal = (Int16)(mFileData[pPos] << 8);
            RetVal += mFileData[pPos + 1];

            return RetVal;
        }

        public UInt16 GetUnsignedWord(int pPos)
        {
            if (mIsFileExist == false)
                throw new FileNotFoundException(mFileName + " file was not read. No data to read!");

            if (pPos + 1 >= mFileData.Length)
                throw new IndexOutOfRangeException();

            return (UInt16)GetWord(pPos);
        }

        public Byte GetByte(int pPos)
        {
            if (mIsFileExist == false)
                throw new FileNotFoundException(mFileName + " file was not read. No data to read!");

            if (pPos >= mFileData.Length)
                throw new IndexOutOfRangeException();

            return (Byte)(mFileData[pPos]);
        }

        public Int32 GetLong(int pPos)
        {
            Int32 RetVal;

            if (mIsFileExist == false)
                throw new FileNotFoundException(mFileName + " file was not read. No data to read!");

            if (pPos >= mFileData.Length)
                throw new IndexOutOfRangeException();

            RetVal = mFileData[pPos + 0] << 24;
            RetVal += mFileData[pPos + 1] << 16;
            RetVal += mFileData[pPos + 2] << 8;
            RetVal += mFileData[pPos + 3];

            return RetVal;
        }

        public UInt32 GetUnsignedLong(int pPos)
        {
            if (mIsFileExist == false)
                throw new FileNotFoundException(mFileName + " file was not read. No data to read!");

            if (pPos + 3 >= mFileData.Length)
                throw new IndexOutOfRangeException();

            return (UInt32)GetLong(pPos);
        }

        public string GetString(int pPos, int pMaxLength)
        {
            if (mIsFileExist == false)
                throw new FileNotFoundException(mFileName + " file was not read. No data to read!");

            if (pPos >= mFileData.Length)
                throw new IndexOutOfRangeException();

            string RetVal = string.Empty;
            int Iterator = 0;

            while (pPos + Iterator < pPos + pMaxLength && mFileData[pPos + Iterator] != 0 && mFileData[pPos + Iterator] != 0xFF)
            {
                RetVal += (char)mFileData[pPos + Iterator];
                Iterator++;
            }

            return RetVal;
        }

        public void Dispose()
        {
            mFileData = null;
        }
    }
}
