﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NfcReader.Connector.GSCFile
{
    public class GSCFileReaderHistory : GSCFileReader
    {
        private string mDrive = string.Empty;
        private string mGSCSN = string.Empty;
        private int HISTORY_0_RECORD_SIZE = 24;
        private int HISTORY_RECORDS_OFFSET = 16;
        private int HISTORY_1_RECORD_SIZE = 32;
        private int MAX_HISTORY_0_EVENTS = 25;
        private int MAX_HISTORY_1_EVENTS = 19;

        //WeaponHistory[] mHistory = null;
        List<object> mHistory = null;


        public GSCFileReaderHistory(byte[] data)
            : base(data)
        {
        }

        public DateTime LastClearDate
        {
            get
            {
                return GetTimestamp(4);
            }
        }

        public Int16 HistoryStartIndex
        {
            get
            {
                Int16 val = GetWord(8);
                if (HistoryType == 0)
                {
                    if (val > HISTORY_0_RECORD_SIZE * MAX_HISTORY_0_EVENTS + HISTORY_RECORDS_OFFSET || val < HISTORY_RECORDS_OFFSET)
                        return (Int16)HISTORY_RECORDS_OFFSET;
                }
                else if (HistoryType == 1)
                {
                    if (val > HISTORY_1_RECORD_SIZE * MAX_HISTORY_1_EVENTS + HISTORY_RECORDS_OFFSET || val < HISTORY_RECORDS_OFFSET)
                        return (Int16)HISTORY_RECORDS_OFFSET;
                }

                return val;
            }
        }

        public Int16 HistoryBufferLength
        {
            get
            {
                if (HistoryType == 0)
                {
                    Int16 val = (Int16)(GetWord(2) - HISTORY_RECORDS_OFFSET);
                    if (val < 0 || val > HISTORY_0_RECORD_SIZE * MAX_HISTORY_0_EVENTS)
                        return 0;

                    return val;
                }
                else if (HistoryType == 1)
                {
                    Int16 val = (Int16)(GetWord(2) - HISTORY_RECORDS_OFFSET);
                    if (val < 0 || val > HISTORY_1_RECORD_SIZE * MAX_HISTORY_1_EVENTS)
                        return 0;

                    return val;
                }

                return 0;
            }
        }

        public byte HistoryType
        {
            get
            {
                return GetByte(10);
            }
        }

        public List<object> History
        {
            get
            {
                try
                {
                    if (mHistory == null)
                    {
                        mHistory = new List<object>();// new WeaponHistory[19];                        
                        int Pos = 0;
                        //                                    
                        for (int i = 0; i < 19; i++)
                        {
                            mHistory.Add(new
                            {
                                Timestamp = GetTimestamp(HISTORY_RECORDS_OFFSET + (Pos * HISTORY_1_RECORD_SIZE)),
                                TotalShotCounter = GetLong(HISTORY_RECORDS_OFFSET + (Pos * HISTORY_1_RECORD_SIZE) + 4),
                                SingleShotCounter = GetLong(HISTORY_RECORDS_OFFSET + (Pos * HISTORY_1_RECORD_SIZE) + 8),
                                BlankShotCounter = GetLong(HISTORY_RECORDS_OFFSET + (Pos * HISTORY_1_RECORD_SIZE) + 12),
                                AverageEnergy = GetWord(HISTORY_RECORDS_OFFSET + (Pos * HISTORY_1_RECORD_SIZE) + 16),
                                AverageGap = GetWord(HISTORY_RECORDS_OFFSET + (Pos * HISTORY_1_RECORD_SIZE) + 18),
                                AverageDurationZone1 = GetWord(HISTORY_RECORDS_OFFSET + (Pos * HISTORY_1_RECORD_SIZE) + 20),
                                AverageZone1Energy = GetWord(HISTORY_RECORDS_OFFSET + (Pos * HISTORY_1_RECORD_SIZE) + 22),
                                AverageVBattery = GetWord(HISTORY_RECORDS_OFFSET + (Pos * HISTORY_1_RECORD_SIZE) + 24),
                                AverageBlankGap = GetWord(HISTORY_RECORDS_OFFSET + (Pos * HISTORY_1_RECORD_SIZE) + 26),
                                AverageDuration = GetWord(HISTORY_RECORDS_OFFSET + (Pos * HISTORY_1_RECORD_SIZE) + 28),
                                MaxG = GetWord(HISTORY_RECORDS_OFFSET + (Pos * HISTORY_1_RECORD_SIZE) + 30)
                            });
                            Pos++;
                            if (Pos > MAX_HISTORY_1_EVENTS)
                                Pos = 0;
                        }
                    }

                    return mHistory;
                }
                catch (Exception ex)
                {
                    return new List<object>();
                }
            }
        }
    }
}
