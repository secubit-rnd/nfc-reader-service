﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NfcReader.Connector.GSCFile
{
    class GSCFileReaderShotCounters : GSCFileReader
    {


        public GSCFileReaderShotCounters(byte[] data)
            : base(data)
        {
        }

        public int TotalShotsCounter
        {
            get
            {
                return GetLong(12);
            }
        }

        public int DryShotsCounter
        {
            get
            {
                return GetLong(20);
            }
        }

        public int SingleShotsCounter
        {
            get
            {
                return GetLong(28);
            }
        }

        public int MultiBurstShotCounter
        {
            get
            {
                return GetLong(36);
            }
        }

        public int BlankMultiBurstShotsCounter
        {
            get
            {
                return GetLong(44);
            }
        }

        public int BlankShotsCounter
        {
            get
            {
                return GetLong(52);
            }
        }

        public int MinShotTime
        {
            get
            {
                return GetWord(56);
            }
        }

        public int AverageShotTime
        {
            get
            {
                return GetWord(58);
            }
        }

        public int MaxShotTime
        {
            get
            {
                return GetWord(60);
            }
        }

        public int MinShotEnergy
        {
            get
            {
                return GetWord(62);
            }
        }

        public int AverageShotEnergy
        {
            get
            {
                return GetWord(64);
            }
        }

        public int MaxShotEnergy
        {
            get
            {
                return GetWord(66);
            }
        }

        public int MinMultiBurstRate
        {
            get
            {
                return GetWord(68);
            }
        }

        public int AverageMultiBurstRate
        {
            get
            {
                return GetWord(70);
            }
        }

        public int MaxMultiBurstRate
        {
            get
            {
                return GetWord(72);
            }
        }

        public int MultiBurstLastFireRate
        {
            get
            {
                return GetWord(80);
            }
        }

        public int WoodMinMultiBurstRate
        {
            get
            {
                return GetWord(74);
            }
        }

        public int WoodAverageMultiBurstRate
        {
            get
            {
                return GetWord(76);
            }
        }

        public int WoodMaxMultiBurstRate
        {
            get
            {
                return GetWord(78);
            }
        }


        public int GetMaxG
        {
            get
            {
                return GetWord(84);
            }
        }
    }
}
