﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NfcReader.Connector.GSCFile
{
    public class GSCFileReaderMfcInfo : GSCFileReader
    {
        public GSCFileReaderMfcInfo(byte[] data)
            : base(data)
        {
        }

        public string GSCPN
        {
            get
            {
                return GetString(8, 12);
            }
        }

        //public string GSCSN
        //{
        //    get
        //    {
        //        return mGSCSN;
        //    }
        //}

        public float GSCHardwareVersion
        {
            get
            {
                return (float)GetWord(32) / (float)100;
            }
        }

        public float GSCSoftwareVersion
        {
            get
            {
                return (float)GetWord(34) / (float)100;
            }
        }

        public DateTime GSCManufacturingDate
        {
            get
            {
                return GetTimestamp(36);
            }
        }

        public int CustomerID
        {
            get
            {
                return GetWord(40);
            }
        }
    }
}
