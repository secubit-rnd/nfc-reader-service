﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NfcReader.Connector.GSCFile
{
    public class GSCFileReaderParameters : GSCFileReader
    {        

        public GSCFileReaderParameters(byte[] data)
            : base(data)
        {
        }

        public byte AED
        {
            get
            {
                return GetByte(8);
            }
        }

        public byte BDF
        {
            get
            {
                return GetByte(9);
            }
        }

        public byte BEF
        {
            get
            {
                return GetByte(10);
            }
        }

        public byte BGF
        {
            get
            {
                return GetByte(11);
            }
        }

        public Int16 AEE
        {
            get
            {
                return GetByte(12);
            }
        }

        public Int16 AEG
        {
            get
            {
                return GetByte(14);
            }
        }

        public Int16 DWL
        {
            get
            {
                return GetByte(16);
            }
        }

        public Int16 HSI
        {
            get
            {
                return GetByte(18);
            }
        }


        public Int16 GDL
        {
            get
            {
                return GetWord(20);
            }
        }

        public byte SENSL
        {
            get
            {
                return GetByte(22);
            }
        }

        public byte NFL
        {
            get
            {
                return GetByte(23);
            }
        }

        public Int16 MBT
        {
            get
            {
                return GetWord(24);
            }
        }

        public Int16 RFC
        {
            get
            {
                return GetWord(26);
            }
        }

        public byte RTCF
        {
            get
            {
                return GetByte(28);
            }
        }

        public byte RTCM
        {
            get
            {
                return GetByte(29);
            }
        }

        public Int16 STH1
        {
            get
            {
                return GetWord(30);
            }
        }

        public Int16 STH2
        {
            get
            {
                return GetWord(32);
            }
        }

        public Int16 SSW
        {
            get
            {
                return GetWord(34);
            }
        }

        public Int16 THIS
        {
            get
            {
                return GetWord(36);
            }
        }

        public Int16 TP1
        {
            get
            {
                return GetWord(38);
            }
        }

        public Int16 TP2
        {
            get
            {
                return GetWord(40);
            }
        }

        public Int16 TS1
        {
            get
            {
                return GetWord(42);
            }
        }

        public Int16 TS2
        {
            get
            {
                return GetWord(44);
            }
        }

        public Int16 TTM
        {
            get
            {
                return GetWord(46);
            }
        }

        public Int16 AvgN
        {
            get
            {
                return GetWord(48);
            }
        }

        public byte DBG
        {
            get
            {
                return GetByte(50);
            }
        }

        public byte GTH
        {
            get
            {
                return GetByte(51);
            }
        }

        public Int16 THIS2
        {
            get
            {
                return GetWord(52);
            }
        }

        public Int16 GOFF
        {
            get
            {
                return GetWord(54);
            }
        }

        public Int16 STHD
        {
            get
            {
                return GetWord(56);
            }
        }

        public byte TTL
        {
            get
            {
                return GetByte(58);
            }
        }

        public byte TTH
        {
            get
            {
                return GetByte(59);
            }
        }

        public byte TGL
        {
            get
            {
                return GetByte(60);
            }
        }

        public byte TGH
        {
            get
            {
                return GetByte(61);
            }
        }

        public byte ESD
        {
            get
            {
                return GetByte(62);
            }
        }

        public byte MBC
        {
            get
            {
                return GetByte(63);
            }
        }

        public Int16 FRD1
        {
            get
            {
                return GetWord(64);
            }
        }

        public Int16 FRD2
        {
            get
            {
                return GetWord(66);
            }
        }

        public Int16 FRD3
        {
            get
            {
                return GetWord(68);
            }
        }

        public Int16 MXPD
        {
            get
            {
                return GetWord(70);
            }
        }

        public byte RTCEN
        {
            get
            {
                return GetByte(72);
            }
        }

        public byte FBFCT
        {
            get
            {
                return GetByte(73);
            }
        }

        public Int16 PKDB
        {
            get
            {
                return GetWord(74);
            }
        }

        public Int16 PKDL
        {
            get
            {
                return GetWord(76);
            }
        }

        public Int16 PKDDB
        {
            get
            {
                return GetWord(78);
            }
        }

        public Int16 PKDDL
        {
            get
            {
                return GetWord(80);
            }
        }

        public byte PKSB
        {
            get
            {
                return GetByte(82);
            }
        }

        public byte PKSL
        {
            get
            {
                return GetByte(83);
            }
        }

        public Int16 TRDT1
        {
            get
            {
                return GetWord(84);
            }
        }

        public Int16 TRDT2
        {
            get
            {
                return GetWord(86);
            }
        }

        public Int16 TRDT3
        {
            get
            {
                return GetWord(88);
            }
        }

        public Int16 TRDT4
        {
            get
            {
                return GetWord(90);
            }
        }

        public Int16 TRDT5
        {
            get
            {
                return GetWord(92);
            }
        }

        public Int16 TRDT6
        {
            get
            {
                return GetWord(94);
            }
        }

    }
}
