﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NfcReader.Connector.GSCFile
{
    public class GSCFileReaderServiceCounters : GSCFileReader
    {
        

        public GSCFileReaderServiceCounters(byte[] data)
            : base(data)
        {
            
        }

        public int FRD1Count
        {
            get
            {
                return GetLong(32);
            }
        }

        public int FRD2Count
        {
            get
            {
                return GetLong(44);
            }
        }

        public int FRD3Count
        {
            get
            {
                return GetLong(56);
            }
        }

        public int FRD4Count
        {
            get
            {
                return GetLong(68);
            }
        }

        public int TRD1Count
        {
            get
            {
                return GetLong(80);
            }
        }

        public int TRD2Count
        {
            get
            {
                return GetLong(92);
            }
        }

        public int TRD3Count
        {
            get
            {
                return GetLong(104);
            }
        }

        public int TRD4Count
        {
            get
            {
                return GetLong(116);
            }
        }

        public int TRD5Count
        {
            get
            {
                return GetLong(128);
            }
        }

        public int TRD6Count
        {
            get
            {
                return GetLong(140);
            }
        }
    }
}
