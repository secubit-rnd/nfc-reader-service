﻿using NfcReader.Common.ExtensionMethods;
using NfcReader.Common.Utils;
using NfcReader.Connector.GSCFile;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NfcReader.Connector.Counter
{
    public class ShotCounters
    {
        public static dynamic Get()
        {
            dynamic result = new ExpandoObject();
            try
            {
                //
                StringBuilder myCmdString = new StringBuilder(256);
                StringBuilder strAnswer = new StringBuilder(256);
                //
                int loop = 128 / 32;
                StringBuilder fullResult = new StringBuilder();
                int cmdResult = 0;
                int start = int.Parse(CommonUtils.GetAppSetting("shotCounters"));//64;
                //                
                for (int i = 0; i < loop; i++)
                {
                    myCmdString = start.ToHex().FormatToConnectorCommand().CreateCommand("07");
                    cmdResult = NfcConnector.CR95HFDll_SendReceive(myCmdString, strAnswer);
                    fullResult.Append(strAnswer.ToString().ParseResponseMessage());
                    start += 8;
                }
                //
                var chars = fullResult.ToString().ToCharArray();
                List<byte> arr = new List<byte>();
                int fullResultLength = (fullResult.ToString().Length / 2);
                for (int i = 0; i < fullResultLength; i++)
                {
                    arr.Add(Convert.ToByte(string.Concat(chars.Skip(2 * i).Take(2).ToList()), 16));
                }
                //
                GSCFileReaderShotCounters shotCounters = new GSCFileReaderShotCounters(arr.ToArray());
                var data = new
                {
                    TotalShotsCounter = shotCounters.TotalShotsCounter,
                    DryShotsCounter = shotCounters.DryShotsCounter,
                    SingleShotsCounter = shotCounters.SingleShotsCounter,
                    MultiBurstShotsCounter = shotCounters.MultiBurstShotCounter,
                    BlankShotsCounter = shotCounters.BlankShotsCounter,
                    BlankMultiBurstShotsCounter = shotCounters.BlankMultiBurstShotsCounter,
                    MinShotTime = shotCounters.MinShotTime,
                    AverageShotTime = shotCounters.AverageShotTime,
                    MaxShotTime = shotCounters.MaxShotTime,
                    MinShotEnergy = shotCounters.MinShotEnergy,
                    AverageShotEnergy = shotCounters.AverageShotEnergy,
                    MaxShotEnergy = shotCounters.MaxShotEnergy,
                    MultiBurstMinFireRate = shotCounters.MinMultiBurstRate,
                    MultiBurstAverageFireRate = shotCounters.AverageMultiBurstRate,
                    MultiBurstMaxFireRate = shotCounters.MaxMultiBurstRate,
                    MultiBurstLastFireRate = shotCounters.MultiBurstLastFireRate,
                    MaxG = shotCounters.GetMaxG
                };
                result.Status = cmdResult;
                result.Answer = data;
                
            }
            catch (Exception ex)
            {
                result.Status = -1;
                result.Answer = ex;
            }
            result.Type = "ShotCounters";
            return result;
        }
    }
}
