﻿using NfcReader.Common.ExtensionMethods;
using NfcReader.Common.Utils;
using NfcReader.Connector.GSCFile;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NfcReader.Connector.Counter
{
    public class History
    {
        public static dynamic Get()
        {
            dynamic result = new ExpandoObject();
            try
            {
                //
                StringBuilder myCmdString = new StringBuilder(256);
                StringBuilder strAnswer = new StringBuilder(256);
                //
                int loop = 640 / 32;
                StringBuilder fullResult = new StringBuilder();
                int cmdResult = 0;
                int start = int.Parse(CommonUtils.GetAppSetting("history"));//64;
                //                
                for (int i = 0; i < loop; i++)
                {
                    myCmdString = start.ToHex().FormatToConnectorCommand().CreateCommand("07");
                    cmdResult = NfcConnector.CR95HFDll_SendReceive(myCmdString, strAnswer);
                    fullResult.Append(strAnswer.ToString().ParseResponseMessage());
                    start += 8;
                }
                //
                var chars = fullResult.ToString().ToCharArray();
                List<byte> arr = new List<byte>();
                int fullResultLength = (fullResult.ToString().Length / 2);
                for (int i = 0; i < fullResultLength; i++)
                {
                    arr.Add(Convert.ToByte(string.Concat(chars.Skip(2 * i).Take(2).ToList()), 16));
                }
                //
                GSCFileReaderHistory history = new GSCFileReaderHistory(arr.ToArray());


                var data = history.History;

                //List<object> datas = new List<object>();
                //for (int i = 0; i < history.History.Count; i++)
                //{
                //    datas.Add(new
                //    {
                //        TotalShotCounter = history.History[i].TotalShotCounter,
                //        Timestamp = history.History[i].Timestamp,
                //        SingleShotCounter = history.History[i].SingleShotCounter,
                //        BlankShotCounter = history.History[i].BlankShotCounter,
                //        AverageEnergy = history.History[i].AverageEnergy,
                //        AverageGap = history.History[i].AverageGap,
                //        AverageBlankGap = history.History[i].AverageBlankGap,
                //        AverageDurationZone1 = history.History[i].AverageDurationZone1,
                //        AverageDuration = history.History[i].AverageDuration,
                //        AverageSDuration = history.History[i].AverageSDuration,
                //        AverageSEnergy = history.History[i].AverageSEnergy,
                //        AverageVBattery = history.History[i].AverageVBattery,
                //        AverageZone0 = history.History[i].AverageZone0,
                //        AverageZone1Energy = history.History[i].AverageZone1Energy,
                //        MaxG = History.history[i].MaxG
                //    });
                //}

                result.Status = cmdResult;
                result.Answer = data;
                
            }
            catch (Exception ex)
            {
                result.Status = -1;
                result.Answer = ex;
            }
            result.Type = "History";
            return result;
        }
    }
}
