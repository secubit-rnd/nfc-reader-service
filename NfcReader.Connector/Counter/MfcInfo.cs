﻿using NfcReader.Common.ExtensionMethods;
using NfcReader.Common.Utils;
using NfcReader.Connector.GSCFile;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NfcReader.Connector.Counter
{
    public class MfcInfo
    {
        public static dynamic Get()
        {
            dynamic result = new ExpandoObject();
            try
            {
                //
                StringBuilder myCmdString = new StringBuilder(256);
                StringBuilder strAnswer = new StringBuilder(256);
                //
                //int loop = 128 / 32;

                string commandPosition = CommonUtils.GetAppSetting("mfcInfo");// "0000";
                StringBuilder fullResult = new StringBuilder();
                int cmdResult = 0;
                //
                //
                myCmdString = commandPosition.FormatToConnectorCommand().CreateCommand("1F");
                cmdResult = NfcConnector.CR95HFDll_SendReceive(myCmdString, strAnswer);
                fullResult.Append(strAnswer.ToString().ParseResponseMessage());

                //for (int i = 0; i < loop; i++)
                //{
                //    myCmdString = start.ToHex().FormatToConnectorCommand().CreateCommand();
                //    cmdResult = NfcConnector.CR95HFDll_SendReceive(myCmdString, strAnswer);
                //    fullResult.Append(strAnswer.ToString().ParseResponseMessage());
                //    start += 8;
                //}
                //
                var chars = fullResult.ToString().ToCharArray();
                List<byte> arr = new List<byte>();
                int fullResultLength = (fullResult.ToString().Length / 2);
                for (int i = 0; i < fullResultLength; i++)
                {
                    arr.Add(Convert.ToByte(string.Concat(chars.Skip(2 * i).Take(2).ToList()), 16));
                }
                //
                GSCFileReaderMfcInfo mfcInfo = new GSCFileReaderMfcInfo(arr.ToArray());
                var data = new
                {
                    CustomerID = mfcInfo.CustomerID,
                    GSCHardwareVersion = mfcInfo.GSCHardwareVersion,
                    GSCSoftwareVersion = mfcInfo.GSCSoftwareVersion,
                    GSCManufacturingDate = mfcInfo.GSCManufacturingDate,
                    GSCPN = mfcInfo.GSCPN
                };
                //
                //result.Data =
                result.Status = cmdResult;
                result.Answer = data;
                

                //return string.Empty;
            }
            catch (Exception ex)
            {

                result.Status = -1;
                result.Answer = ex;
            }
            result.Type = "MfcInfo";
            return result;
        }
    }
}
