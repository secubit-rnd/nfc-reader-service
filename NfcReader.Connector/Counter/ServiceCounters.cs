﻿using NfcReader.Common.ExtensionMethods;
using NfcReader.Common.Utils;
using NfcReader.Connector.GSCFile;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NfcReader.Connector.Counter
{
    public class ServiceCounters
    {
        public static dynamic Get()
        {
            dynamic result = new ExpandoObject();
            try
            {
                //
                StringBuilder myCmdString = new StringBuilder(256);
                StringBuilder strAnswer = new StringBuilder(256);
                //
                int loop = 256 / 32;
                StringBuilder fullResult = new StringBuilder();
                int cmdResult = 0;
                int start = int.Parse(CommonUtils.GetAppSetting("serviceCounters"));// 96;
                //                
                for (int i = 0; i < loop; i++)
                {
                    myCmdString = start.ToHex().FormatToConnectorCommand().CreateCommand("07");
                    cmdResult = NfcConnector.CR95HFDll_SendReceive(myCmdString, strAnswer);
                    fullResult.Append(strAnswer.ToString().ParseResponseMessage());
                    start += 8;
                }
                //
                var chars = fullResult.ToString().ToCharArray();
                List<byte> arr = new List<byte>();
                int fullResultLength = (fullResult.ToString().Length / 2);
                for (int i = 0; i < fullResultLength; i++)
                {
                    arr.Add(Convert.ToByte(string.Concat(chars.Skip(2 * i).Take(2).ToList()), 16));
                }
                //
                GSCFileReaderServiceCounters serviceCounters = new GSCFileReaderServiceCounters(arr.ToArray());
                var data = new
                {
                    FRD1Counter = serviceCounters.FRD1Count,
                    FRD2Counter = serviceCounters.FRD2Count,
                    FRD3Counter = serviceCounters.FRD3Count,
                    FRD4Counter = serviceCounters.FRD4Count,
                    TRD1Counter = serviceCounters.TRD1Count,
                    TRD2Counter = serviceCounters.TRD2Count,
                    TRD3Counter = serviceCounters.TRD3Count,
                    TRD4Counter = serviceCounters.TRD4Count,
                    TRD5Counter = serviceCounters.TRD5Count,
                    TRD6Counter = serviceCounters.TRD6Count
                };
                result.Status = cmdResult;
                result.Answer = data;
                
            }
            catch (Exception ex)
            {
                result.Status = -1;
                result.Answer = ex;
            }
            result.Type = "ServiceCounters";
            return result;
        }
    }
}
