﻿using NfcReader.Common.ExtensionMethods;
using NfcReader.Common.Utils;
using NfcReader.Connector.GSCFile;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NfcReader.Connector.Counter
{
    public class SysInfo
    {
        public static dynamic Get()
        {
            dynamic result = new ExpandoObject();
            try
            {
                //
                StringBuilder myCmdString = new StringBuilder(256);
                StringBuilder strAnswer = new StringBuilder(256);
                //
                int loop = 128 / 32;
                StringBuilder fullResult = new StringBuilder();
                int cmdResult = 0;
                int start = int.Parse(CommonUtils.GetAppSetting("sysInfo"));//32;
                //                
                for (int i = 0; i < loop; i++)
                {
                    myCmdString = start.ToHex().FormatToConnectorCommand().CreateCommand("07");
                    cmdResult = NfcConnector.CR95HFDll_SendReceive(myCmdString, strAnswer);
                    fullResult.Append(strAnswer.ToString().ParseResponseMessage());
                    start += 8;
                }
                //
                var chars = fullResult.ToString().ToCharArray();
                List<byte> arr = new List<byte>();
                int fullResultLength = (fullResult.ToString().Length / 2);
                for (int i = 0; i < fullResultLength; i++)
                {
                    arr.Add(Convert.ToByte(string.Concat(chars.Skip(2 * i).Take(2).ToList()), 16));
                }
                //
                GSCFileReaderSysInfo sysInfo = new GSCFileReaderSysInfo(arr.ToArray());
                var data = new
                {
                    GSCCurrentDate = DateTime.Now,
                    GSCInitiationDate = sysInfo.GSCInitiationDate,
                    GSCRTCUpdateDate = sysInfo.GSCRTCUpdateDate,
                    WeaponBatteryLevel = sysInfo.GSCBatteryLevel,
                    WeaponType = sysInfo.WeaponType,
                    WeaponSubType = sysInfo.WeaponSubType,
                    WeaponSerialNumber = sysInfo.WeaponSerialNumber,
                    SoldierID = sysInfo.SoldierID,
                    SoldierName = sysInfo.SoldierName
                };
                result.Status = cmdResult;
                result.Answer = data;

            }
            catch (Exception ex)
            {
                result.Status = -1;
                result.Answer = ex;
            }
            result.Type = "SysInfo";
            return result;
        }
    }
}
