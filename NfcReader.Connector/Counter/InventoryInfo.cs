﻿using NfcReader.Connector.Board;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NfcReader.Connector.Counter
{
    public class InventoryInfo
    {
        public static dynamic Get()
        {
            dynamic result = new ExpandoObject();
            try
            {
                var connectStatus = ConnectBoard.Connect();
                //
                StringBuilder myCmdString = new StringBuilder(256).Append("260100");
                StringBuilder strAnswer = new StringBuilder(256);
                int cmdResult = NfcConnector.CR95HFDll_SendReceive(myCmdString, strAnswer);
                //
                result.Status = cmdResult;
                result.Answer = strAnswer.ToString();
                //return string.Empty;
            }
            catch (Exception ex)
            {
                result.Status = -1;
                result.Answer = ex.ToString();
            }
            return result;
        }


        //public static void AAAA()
        //{
        //    var re = Get();
        //    int a = re?.Status;
        //}


    }
    //public class AAAA
    //{
    //    public int Status { get; set; }
    //    public string Answer { get; set; }
    //}
}
