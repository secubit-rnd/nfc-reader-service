﻿using NfcReader.Common.ExtensionMethods;
using NfcReader.Common.Utils;
using NfcReader.Connector.GSCFile;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NfcReader.Connector.Counter
{
    public class Parameters
    {
        public static dynamic Get()
        {
            dynamic result = new ExpandoObject();
            try
            {
                //
                StringBuilder myCmdString = new StringBuilder(256);
                StringBuilder strAnswer = new StringBuilder(256);
                //
                int loop = 256 / 32;
                StringBuilder fullResult = new StringBuilder();
                int cmdResult = 0;
                int start = int.Parse(CommonUtils.GetAppSetting("parameters"));//160;
                //                
                for (int i = 0; i < loop; i++)
                {
                    myCmdString = start.ToHex().FormatToConnectorCommand().CreateCommand("07");
                    cmdResult = NfcConnector.CR95HFDll_SendReceive(myCmdString, strAnswer);
                    fullResult.Append(strAnswer.ToString().ParseResponseMessage());
                    start += 8;
                }
                //
                var chars = fullResult.ToString().ToCharArray();
                List<byte> arr = new List<byte>();
                int fullResultLength = (fullResult.ToString().Length / 2);
                for (int i = 0; i < fullResultLength; i++)
                {
                    arr.Add(Convert.ToByte(string.Concat(chars.Skip(2 * i).Take(2).ToList()), 16));
                }
                //
                GSCFileReaderParameters parameters = new GSCFileReaderParameters(arr.ToArray());
                var data = new
                {
                    AED = parameters.AED,
                    AEE = parameters.AEE,
                    AEG = parameters.AEG,
                    AvgN = parameters.AvgN,
                    BDF = parameters.BDF,
                    BEF = parameters.BEF,
                    BGF = parameters.BGF,
                    DBG = parameters.DBG,
                    DWL = parameters.DWL,
                    GDL = parameters.GDL,
                    HSI = parameters.HSI,
                    SENSL = parameters.SENSL,
                    MBT = parameters.MBT,
                    NFL = parameters.NFL,
                    RFC = parameters.RFC,
                    RTCF = parameters.RTCF,
                    RTCM = parameters.RTCM,
                    SSW = parameters.SSW,
                    STH1 = parameters.STH1,
                    STH2 = parameters.STH2,
                    THIS = parameters.THIS,
                    TP1 = parameters.TP1,
                    TP2 = parameters.TP2,
                    TS1 = parameters.TS1,
                    TS2 = parameters.TS2,
                    TTM = parameters.TTM,
                    GTH = parameters.GTH,
                    THIS2 = parameters.THIS2,
                    GOFF = parameters.GOFF,
                    STHD = parameters.STHD,
                    TTL = parameters.TTL,
                    TTH = parameters.TTH,
                    TGL = parameters.TGL,
                    TGH = parameters.TGH,
                    ESD = parameters.ESD,
                    MBC = parameters.MBC,
                    FRD1 = parameters.FRD1,
                    FRD2 = parameters.FRD2,
                    FRD3 = parameters.FRD3,
                    MXPD = parameters.MXPD,
                    RTCEN = parameters.RTCEN,
                    FBFCT = parameters.FBFCT,
                    PKDB = parameters.PKDB,
                    PKDL = parameters.PKDL,
                    PKDDB = parameters.PKDDB,
                    PKDDL = parameters.PKDDL,
                    PKSB = parameters.PKSB,
                    PKSL = parameters.PKSL
                };
                result.Status = cmdResult;
                result.Answer = data;
                
            }
            catch (Exception ex)
            {
                result.Status = -1;
                result.Answer = ex;
            }
            result.Type = "Parameters";
            return result;
        }
    }
}
