﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace NfcReader.Connector
{
    public class NfcConnector
    {
        //get dll revision
        [DllImport("CR95HF.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int CR95HFDll_GetDLLrev(StringBuilder strAnswer);

        //connect
        [DllImport("CR95HF.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern long CR95HFDLL_USBconnect();

        //check usb
        [DllImport("CR95HF.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int CR95HFDLL_USBhandlecheck();

        //
        [DllImport("CR95HF.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int CR95HFDll_Idn(StringBuilder strAnswer);
        //
        [DllImport("CR95HF.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int CR95HFDll_Select(StringBuilder myCmdString, StringBuilder strSelectMsg);
        //
        [DllImport("CR95HF.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int CR95HFDll_SendReceive(StringBuilder mycmdstring, byte[] strAnswer);

        [DllImport("CR95HF.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int CR95HFDll_SendReceive(StringBuilder mycmdstring, StringBuilder strAnswer);


        [DllImport("CR95HF.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int CR95HFDLL_getInterfacePinState(StringBuilder strAnswer);

        [DllImport("CR95HF.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int CR95HFDll_Echo(StringBuilder strAnswer);
        //

        [DllImport("CR95HF.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern long CR95HFDll_Polling_Reading(string strAnswer);

        [DllImport("CR95HF.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int CR95HFDll_SendNSSPulse(StringBuilder strAnswer);
    }
}
