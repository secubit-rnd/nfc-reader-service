﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NfcReader.Connector.Board
{
    public class ConnectBoard
    {
        public static dynamic Connect()
        {
            dynamic result = new ExpandoObject();
            try
            {
                StringBuilder msg = new StringBuilder();
                long resultStatus = NfcConnector.CR95HFDLL_USBconnect();
                if (result != 0)
                {
                    result.Status = (int)resultStatus;
                    result.Answer = "CR95HF demonstration board not connected.";
                    return result;
                }
                //
                resultStatus = NfcConnector.CR95HFDLL_USBhandlecheck();
                if (result != 0)
                {
                    result.Status = (int)resultStatus;
                    result.Answer = "CR95HF demonstration board not connected.";
                    return result;
                }

                //            
                StringBuilder selectAnswer = new StringBuilder(256);
                StringBuilder myCmdString = new StringBuilder(256);
                myCmdString.Append("010D");
                resultStatus = NfcConnector.CR95HFDll_Select(myCmdString, selectAnswer);
                if (result != 0)
                {
                    result.Status = (int)resultStatus;
                    result.Answer = "CR95HF demonstration board not connected.";
                    return result;
                }

                result.Status = (int)resultStatus;
                result.Answer = "CR95HF connected.";


            }
            catch (Exception ex)
            {
                result.Status = -1;
                result.Answer = ex.ToString();
            }
            return result;
        }
    }
}
