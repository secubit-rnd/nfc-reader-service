﻿namespace NfcReader.Service
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.NFCServiceProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.NFCServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // NFCServiceProcessInstaller
            // 
            this.NFCServiceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.NFCServiceProcessInstaller.Password = null;
            this.NFCServiceProcessInstaller.Username = null;
            // 
            // NFCServiceInstaller
            // 
            this.NFCServiceInstaller.Description = "Secubit Nfc Reader Service";
            this.NFCServiceInstaller.ServiceName = "Nfc Reader Service";
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.NFCServiceProcessInstaller,
            this.NFCServiceInstaller});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller NFCServiceProcessInstaller;
        private System.ServiceProcess.ServiceInstaller NFCServiceInstaller;
    }
}