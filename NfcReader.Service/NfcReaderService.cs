﻿using Microsoft.Owin.Hosting;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using CustomTimer;
using System.Timers;
using NfcReader.Connector.Counter;
using System.Threading;

namespace NfcReader.Service
{
    partial class NfcReaderService : ServiceBase
    {
        private CustomTimer.CustomTimer timer { get; set; }
        public NfcReaderService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            string baseAddress = $"http://localhost:{ConfigurationManager.AppSettings["ServerPort"].ToString()}/";
            // Start OWIN host 
            WebApp.Start<Startup>(url: baseAddress);
            //pooling timer            
            timer = new CustomTimer.CustomTimer();
            timer.Interval = int.Parse(ConfigurationManager.AppSettings["ServiceInterval"]);
            timer.Elapsed += Timer_Elapsed;
            timer.AutoReset = false;
            timer.Start();
        }

        private void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                using (EventLog eventLog = new EventLog("Application"))
                {
                    eventLog.Source = "Application";
                    var inventory = InventoryInfo.Get();
                    if (inventory.Status != 0)
                    {
                        eventLog.WriteEntry("Cannot Read");
                    }
                    else
                    {
                        eventLog.WriteEntry($"Read {inventory.Answer}");
                    }

                }

            }
            catch (Exception ex)
            {

            }
            timer.AutoReset = true;
        }

        protected override void OnStop()
        {

        }
    }
}
