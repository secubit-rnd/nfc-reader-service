﻿using Newtonsoft.Json.Serialization;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Dispatcher;

namespace NfcReader.Service
{
    public class Startup
    {
        // This code configures Web API. The Startup class is specified as a type
        // parameter in the WebApp.Start method.
        public void Configuration(IAppBuilder appBuilder)
        {
            //Thread.Sleep(20000);
            // Configure Web API for self-host. 
            HttpConfiguration config = new HttpConfiguration();

            var assembly = System.Reflection.Assembly.GetExecutingAssembly().Location;
            //NfcReader.Api
            string path = assembly.Substring(0, assembly.LastIndexOf("\\")) + "\\NfcReader.Api.dll";
            config.Services.Replace(typeof(IAssembliesResolver), new SelfHostAssemblyResolver(path));


            // Web API configuration and services
            config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            config.Formatters.JsonFormatter.UseDataContractJsonSerializer = false;
            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
            EnableCors(config);
            //
            appBuilder.UseWebApi(config);
        }

        public static void EnableCors(HttpConfiguration config)
        {
            EnableCorsAttribute cors = new EnableCorsAttribute("*", "*", "*");
            config.EnableCors(cors);
        }
    }
}
