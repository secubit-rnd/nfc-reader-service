﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NfcReader.Common.ExtensionMethods
{
    public static class IntExtensions
    {
        public static string ToHex(this int value)
        {
            string hexValue = value.ToString("X4");
            return hexValue;
        }
    }
}
