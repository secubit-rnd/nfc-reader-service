﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NfcReader.Common.ExtensionMethods
{
    public static class StringExtensions
    {
        public static string FormatToConnectorCommand(this string hexValue)
        {
            return string.Format("{0}{1}", hexValue.Substring(2), hexValue.Substring(0, 2));
        }

        public static StringBuilder CreateCommand(this string format, string length)
        {
            return new StringBuilder($"0A23{format}{length}");
        }

        public static string ParseResponseMessage(this string msg)
        {
            try
            {
                string cmdResultAsString = msg.Substring(6);
                return cmdResultAsString.Substring(0, (cmdResultAsString.Length - 6));
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }
    }
}
