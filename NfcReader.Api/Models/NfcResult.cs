﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace NfcReader.Api.Models
{
    public class NfcResult
    {
        public HttpStatusCode StatusCode { get; set; }
        public object Data { get; set; }
        //public int Status { get; set; }
        //public string Answer { get; set; }
    }
}