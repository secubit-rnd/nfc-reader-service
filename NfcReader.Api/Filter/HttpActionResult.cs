﻿using NfcReader.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace NfcReader.Api.Filter
{
    public class HttpActionResult : IHttpActionResult
    {
        private readonly HttpRequestMessage _request;
        private readonly NfcResult _resp;


        public HttpActionResult(HttpRequestMessage request, NfcResult resp)
        {
            _request = request;
            _resp = resp;
        }
        public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            var response = _request.CreateResponse(_resp.StatusCode, _resp.Data);
            return Task.FromResult(response);
        }
    }
}