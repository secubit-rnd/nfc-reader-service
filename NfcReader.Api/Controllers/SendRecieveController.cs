﻿using NfcReader.Api.Filter;
using NfcReader.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NfcReader.Api.Controllers
{
    public class SendRecieveController : ApiController
    {
        public IHttpActionResult Post()
        {
            NfcResult result = new NfcResult();
            try
            {
                result.StatusCode = HttpStatusCode.OK;

            }
            catch (Exception ex)
            {
                result.StatusCode = HttpStatusCode.InternalServerError;
            }
            return new HttpActionResult(Request, result);
        }
    }
}
