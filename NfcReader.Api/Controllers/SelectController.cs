﻿using NfcReader.Api.Filter;
using NfcReader.Api.Models;
using NfcReader.Connector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;

namespace NfcReader.Api.Controllers
{
    public class SelectController : ApiController
    {
        public IHttpActionResult Get()
        {
            NfcResult result = new NfcResult();
            try
            {
                result.StatusCode = HttpStatusCode.OK;
                StringBuilder selectAnswer = new StringBuilder(256);
                StringBuilder myCmdString = new StringBuilder(256);
                myCmdString.Append("010D");
                result.Data = new
                {
                    Status = NfcConnector.CR95HFDll_Select(myCmdString, selectAnswer),
                    Answer = selectAnswer.ToString()
                };
                result.StatusCode = HttpStatusCode.OK;
                return new HttpActionResult(Request, result);
            }
            catch (Exception ex)
            {
                result.StatusCode = HttpStatusCode.InternalServerError;
                result.Data = ex;
                return new HttpActionResult(Request, result);
            }           
        }
    }
}
