﻿using NfcReader.Api.Filter;
using NfcReader.Api.Models;
using NfcReader.Connector;
using NfcReader.Connector.Counter;
using NfcReader.Connector.GSCFile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;

namespace NfcReader.Api.Controllers
{
    public class MfcInfoController : NfcReaderController
    {
        public IHttpActionResult Get()
        {
            NfcResult resp = new NfcResult();
            //
            var info = MfcInfo.Get();
            resp.StatusCode = info.Status == 0 ? HttpStatusCode.OK : HttpStatusCode.InternalServerError;
            //
            resp.Data = info;
            return new HttpActionResult(Request, resp);
        }
    }
}
