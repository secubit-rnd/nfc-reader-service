﻿using NfcReader.Api.Models;
using NfcReader.Connector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using NfcReader.Connector.Board;

namespace NfcReader.Api.Controllers
{
    public class NfcReaderController : ApiController
    {
        public override Task<HttpResponseMessage> ExecuteAsync(HttpControllerContext controllerContext, CancellationToken cancellationToken)
        {
            //var board = ConnectBoard.Connect();
            SendRecieveModel openEncCommand = new SendRecieveModel
            {
                Command = "02B30201C7B45CE4"
            };
            StringBuilder myCmdString = new StringBuilder(256).Append(openEncCommand.Command);
            StringBuilder strAnswer = new StringBuilder(256);
            //            
            int cmdResult = NfcConnector.CR95HFDll_SendReceive(myCmdString, strAnswer);
            //


            //SendRecieveModel openEncCommand = new SendRecieveModel
            //{
            //    Command = "02B30201C7B45CE4"
            //};
            //StringBuilder myCmdString = new StringBuilder(256).Append(openEncCommand.Command);
            //StringBuilder strAnswer = new StringBuilder(256);
            //return Task.FromResult(controllerContext.Request.CreateResponse(HttpStatusCode.OK, string.Empty));            

            return base.ExecuteAsync(controllerContext, cancellationToken);
        }
    }
}
