﻿using NfcReader.Api.Filter;
using NfcReader.Api.Models;
using NfcReader.Connector.Counter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NfcReader.Api.Controllers.Nfc
{
    public class ShotCountersController : NfcReaderController
    {
        public IHttpActionResult Get()
        {
            NfcResult resp = new NfcResult();
            //
            var info = ShotCounters.Get();
            resp.StatusCode = info.Status == 0 ? HttpStatusCode.OK : HttpStatusCode.InternalServerError;
            //
            resp.Data = info;
            return new HttpActionResult(Request, resp);
        }
    }
}
