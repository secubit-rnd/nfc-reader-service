﻿using NfcReader.Api.Filter;
using NfcReader.Api.Models;
using NfcReader.Connector.Counter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NfcReader.Api.Controllers.Nfc
{
    public class SysInfoController : NfcReaderController
    {
        public IHttpActionResult Get()
        {

            NfcResult resp = new NfcResult();
            //
            var info = SysInfo.Get();
            resp.StatusCode = info.Status == 0 ? HttpStatusCode.OK : HttpStatusCode.InternalServerError;
            //
            resp.Data = info;
            return new HttpActionResult(Request, resp);

            //NfcResult result = new NfcResult();
            //try
            //{
            //    result.StatusCode = HttpStatusCode.OK;
            //    //
            //    result.Data = SysInfo.Get();
            //}
            //catch (Exception ex)
            //{
            //    result.StatusCode = HttpStatusCode.InternalServerError;
            //}
            //return new HttpActionResult(Request, result);
        }
    }
}
