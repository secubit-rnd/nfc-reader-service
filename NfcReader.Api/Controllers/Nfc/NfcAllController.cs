﻿using NfcReader.Api.Filter;
using NfcReader.Api.Models;
using NfcReader.Connector.Counter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NfcReader.Api.Controllers.Nfc
{
    public class NfcAllController : NfcReaderController
    {
        public IHttpActionResult Get()
        {
            NfcResult resp = new NfcResult();
            //
            List<dynamic> all = new List<dynamic>();

            all.Add(MfcInfo.Get());
            all.Add(SysInfo.Get());
            all.Add(ShotCounters.Get());
            all.Add(ServiceCounters.Get());
            all.Add(Parameters.Get());
            all.Add(History.Get());

            var isError = all.FirstOrDefault(x => x.Status == -1);
            if (isError != null)
            {
                resp.StatusCode = HttpStatusCode.InternalServerError;
                resp.Data = isError;
            }
            else
            {
                resp.StatusCode = HttpStatusCode.OK;
                //
                foreach (var data in all)
                {

                }
                resp.Data = all;
            }
            return new HttpActionResult(Request, resp);
        }
    }
}
