﻿using NfcReader.Api.Filter;
using NfcReader.Api.Models;
using NfcReader.Connector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;

namespace NfcReader.Api.Controllers
{
    public class ConnectController : NfcReaderController
    {
        public IHttpActionResult Get()
        {            
            NfcResult totalResult = new NfcResult();
            totalResult.StatusCode = HttpStatusCode.OK;
            //totalResult.Status = 0;
            //
            StringBuilder msg = new StringBuilder();
            long result = NfcConnector.CR95HFDLL_USBconnect();
            if (result != 0)
            {
                totalResult.StatusCode = HttpStatusCode.InternalServerError;
                totalResult.Data = new
                {
                    Status = (int)result,
                    Answer = "CR95HF demonstration board not connected."
                };
                return new HttpActionResult(Request, totalResult);
            }
            //
            result = NfcConnector.CR95HFDLL_USBhandlecheck();
            if (result != 0)
            {
                totalResult.StatusCode = HttpStatusCode.InternalServerError;
                totalResult.Data = new
                {
                    Status = (int)result,
                    Answer = "The USB handle is not valid."
                };
                return new HttpActionResult(Request, totalResult);
            }

            //            
            StringBuilder selectAnswer = new StringBuilder(256);
            StringBuilder myCmdString = new StringBuilder(256);
            myCmdString.Append("010D");
            result = NfcConnector.CR95HFDll_Select(myCmdString, selectAnswer);
            if (result != 0)
            {
                totalResult.StatusCode = HttpStatusCode.InternalServerError;
                totalResult.Data = new
                {
                    Status = (int)result,
                    Answer = "Cannot set select 010D"
                };
                return new HttpActionResult(Request, totalResult);
            }
            return new HttpActionResult(Request, totalResult);
        }
    }
}
