﻿using NfcReader.Api.Filter;
using NfcReader.Api.Models;
using NfcReader.Connector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NfcReader.Api.Controllers
{
    public class USBHandleCheckController : ApiController
    {
        public IHttpActionResult Get()
        {
            NfcResult result = new NfcResult();
            try
            {
                result.StatusCode = HttpStatusCode.OK;
                result.Data = new
                {
                    Status = NfcConnector.CR95HFDLL_USBhandlecheck()
                };
                result.StatusCode = HttpStatusCode.OK;
                return new HttpActionResult(Request, result);
            }
            catch (Exception ex)
            {
                result.StatusCode = HttpStatusCode.InternalServerError;
                result.Data = ex;
                return new HttpActionResult(Request, result);
            }
        }
    }
}
